#/bin/sh

# variables

img_name="atlas_postgres"
admin_pass="22222"

echo "|||||||||||||| Start PostgreSQL ||||||||||||"

if [ "$1" == "" ]
then
	echo "run with <dir_name>"
	exit 1
fi

echo "------------------------- Download image..."

docker pull postgres:10

echo "------------------------- Starting image..."

# start image
docker run -d \
        -p 5432:5432 \
        -e POSTGRES_PASSWORD=$admin_pass \
        --name $img_name \
	-v $1:/var/lib/postgresql/data \
        postgres:10 docker-entrypoint.sh postgres

# if unsucs
if [ "$?" != "0" ]
then
	docker start $img_name
	exit 1
fi

echo "--------------------------- All"
echo "$1 is a persistent dir"
echo "$img_name is name of image"
