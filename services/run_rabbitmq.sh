#/bin/sh

# variables

img_name="atlas_rabbit"
admin_name="guest"
admin_pass="guest"
worker_name="atlas_worker"
worker_pass="22222"

echo "|||||||||||||| Start RabbitMQ ||||||||||||"

if [ "$1" == "" ]
then
	echo "run with <dir_name>"
	exit 1
fi

echo "------------------------- Download image..."

docker pull rabbitmq

echo "------------------------- Starting image..."

# start image
docker run -d \
	--hostname my-rabbit \
	--name $img_name \
	-p 5672:5672 -p 15672:15672 \
	-e "RABBITMQ_LOGS=/var/log/rabbitmq/rabbit.log" \
        -v $1:/var/lib/rabbitmq \
        -v $1/logs:/var/log/rabbitmq \
	rabbitmq:management

# if unsucs
if [ "$?" != "0" ]
then
	docker start $img_name
	exit 1
fi

echo "------------------------- Wait starting (around 38~60s)..."

is_started=0
while [ "$is_started" == "0" ]
do
	curl --connect-timeout 5 -f localhost:15672 > /dev/null 2>/dev/null
	if [ "$?" == "0" ]
	then
		is_started=1
	fi
done

echo "------------------------- Setup users..."

# setup users
docker exec $img_name rabbitmqctl start_app
docker exec $img_name rabbitmqctl delete_user guest
docker exec $img_name rabbitmqctl add_vhost /
docker exec $img_name rabbitmqctl add_user $admin_name $admin_pass
docker exec $img_name rabbitmqctl set_user_tags $admin_name administrator
docker exec $img_name rabbitmqctl set_permissions -p / $admin_name ".*" ".*" ".*"
docker exec $img_name rabbitmqctl add_user $worker_name $worker_pass
docker exec $img_name rabbitmqctl set_permissions -p / $worker_name ".*" ".*" ".*"

echo "--------------------------- All"
echo "$1 is a persistent dir"
echo "$1/logs is a logs of RabbitMQ"
echo "$img_name is name of image"
echo "15672 is a GUI port"
