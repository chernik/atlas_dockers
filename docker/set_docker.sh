#/bin/sh

echo "||||||||||||||||||||||||||||||||"
echo "set_docker.sh started"

# apk
echo "Upgrade APK..."
apk update
apk upgrade
echo "---------------------------------"

# python
echo "Install python & virtualenv..."
apk add python3 py-pip
pip install --upgrade pip
pip install venv
echo "---------------------------------"

# for psycopg2
echo "Install gcc for psycopg2"
apk add --virtual .for-psycopg2 \
	gcc \
	python-dev python2-dev python3-dev \
	musl-dev postgresql-dev libffi-dev
echo "---------------------------------"

# git repo
echo "Install git..."
apk add git
git config --global user.email ""
git config --global user.name "docker"
echo "Clone repo..."
git clone https://chernik_rdonly:12345678@bitbucket.org/chernik/atlas.git ~/atlas
cd ~/atlas
echo "--------------------------------"
echo "install.sh started"
echo "||||||||||||||||||||||||||||||||"
./install.sh
echo "||||||||||||||||||||||||||||||||"
echo "The end"

# more things
apk add p7zip
