#/bin/sh

name_image=img_atlas_django
name_container=atlas_django
name_worlddir="/home/user/7z"
cmd_stop="docker rm -f $name_container"

echo "-----------------------"

if [ "$1" == "-rm" ]
then
	rm change_dynsets.sh
	rm pull_docker.sh
	rm for_pull_docker.sh
	rm run_collector.sh
	echo "Removed!"
	exit 0
fi

added_opts=""
if [ "$1" != "" ] && [ "$1" != "null" ]
then
	added_opts="$added_opts --mount src=$1,target=$name_worlddir,type=bind"
	echo "Dir '$1' will be mounted as '$name_worlddir'"
fi
echo "$added_opts"

is_install=1
if [ "$(docker images | grep $name_image)" != "" ]
then
	read -t 10 -p "If you don't want to reload docker image, put empty string or wait 10s > " inp
	if [ "$inp" == "" ]
	then
		is_install=0
	fi
fi

if [ $is_install == 1 ]
then
	echo "Building docker..."
	echo "||||||||||||||||||||||"
	docker build --no-cache -t $name_image .
	echo "||||||||||||||||||||||"
fi

if [ "$(docker ps | grep $name_container)" != "" ]
then
	echo "Docker alreay run! Stop it firstly"
else
	echo "Start docker..."
	docker run --name=$name_container \
		-p=8000:8000 \
		$added_opts \
		-d $name_image \
		-e ATLAS_DATABASE_HOST="atlas.ipgg.sbras.ru" \
		-e ATLAS_QUEUE_HOST="atlas.ipgg.sbras.ru" \
		sh /root/atlas/run.sh
	if [ $? != 0 ]
	then
		docker start $name_container
	fi
	echo "---------------------"
	echo "Sucs!"
fi
echo ""
echo "For stop put 'docker rm -f $name_container'"
echo "For attach put 'docker exec -it $name_container sh'"
echo "For start run this script again and put empty string"
echo "For pull changes run 'pull_docker.sh'"

cat <<EOF > pull_docker.sh
#/bin/sh
docker cp for_pull_docker.sh $name_container:/root/buf.sh
docker exec $name_container sh /root/buf.sh \$1
ret=\$?
if [ \$ret != 0 ]
then
	exit \$ret
fi
docker restart $name_container
EOF
chmod +x pull_docker.sh

cat <<EOF > for_pull_docker.sh
#/bin/sh
if [ "\$1" == "" ]
then
	checkout="master"
else
	checkout="\$1"
fi
cd /root/atlas
git checkout \$checkout
fl=1
while [ "\$fl" == "1" ]
do
	git reset --hard
	git pull
	if [ "\$?" == "0" ]
	then
		fl=0
	fi
done
./install.sh
exit \$?

EOF
chmod +x for_pull_docker.sh
