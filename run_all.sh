#/bin/sh
cd "${0%/*}"

if [ "$1" == "" ]
then
	echo "run_all.sh <pers_dir> <branch>"
	echo "<pers_dir> contains >"
	echo "7z - for django"
	echo "psql - for postgres"
	echo "rabbit - for rabbitMQ"
	exit 0
fi

echo ">>>>>>>>>>>>>>>>>>> POSTGRES <<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
services/run_postgresql.sh "$1/psql"
echo
echo

echo ">>>>>>>>>>>>>>>>>>>> RABBIT <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
services/run_rabbitmq.sh "$1/rabbit"
echo
echo

echo ">>>>>>>>>>>>>>>>>>>> DJANGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
cd docker
./run_docker.sh "$1/7z"
echo
echo

echo ">>>>>>>>>>>>>>>>>> PULL DJANGO <<<<<<<<<<<<<<<<<<<<<<<<<<<"
./pull_docker.sh $2
echo
echo

echo ">>>>>>>>>>>>>>>>>>>>>> ALL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
